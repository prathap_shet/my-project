# README #

### What is this repository for? ###

* My project consists of login page using google signin, once signed in the page lands on a different page named myAccount.html where the user can enter any text on an input field and submit it, after submitting user gets a response with a text entered above along with date and time.

### How do I get set up? ###

* Install an Apache Server and php along with Mysql for database.
* Copy the project directory onto your root server.
* import the sql file provide within the project.
* Run the project using localhost server.
